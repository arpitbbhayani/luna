#!/bin/bash

set -e

cd temp
cp -r ../theme/silvernik/* ./semantic/src/themes/default

REPOROOT=/Users/arpit/myw/arpitbhayani.me

cd semantic
../node_modules/.bin/gulp build
cp dist/semantic.min.css $REPOROOT/static/static/css/semantic.min.css
cp -r dist/themes/default/assets/* $REPOROOT/static/static/
